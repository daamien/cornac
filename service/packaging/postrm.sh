#!/bin/bash -eux

if systemctl is-system-running &>/dev/null ; then
	systemctl daemon-reload
fi
