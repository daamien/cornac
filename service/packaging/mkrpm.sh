#!/bin/bash -eux
#
# Usage: mkrpm dist/pgCornac*.whl
#
# Wraps cornac bundle in a rpm.
#

dist=${1-pgCornac}
pkgdir=$(readlink -m $0/..)
top_srcdir=$(readlink -m $0/../..)
distdir="${top_srcdir}/dist"
workdir=$(mktemp --directory -t cornac-rpm-XXX)

# Test for build requirements.
type -p fpm
type -p rpmbuild
type -p yum

teardown () {
	if [ "0" = "${CLEAN-1}" ] ; then
		return
	fi

	rm -rf $workdir
	yum remove -qy cornac

	if ! [ -f "${rpm-}" ] ; then
		return
	fi

	# Now display a nice message on success.
	set +x
	cat <<-EOF


	 	RPM package is available at $rpm.


	EOF
}
trap teardown EXIT INT TERM

# If dist is a filename, compute its absolute path.
if [ -z "${dist%*pgCornac-*.*}" ] ; then
	# Compute root for relative path.
	if [ -n "${COMPOSEBUILD-}" ] ; then
		# Compose build is relative to docker-compose.yml directory.
		callerwd="$(readlink -m $0/..)"
	fi
	dist=$(cd "${callerwd-.}"; readlink -e "$dist")
	test -f "$dist"
fi

# BUILD

export DESTDIR=${workdir}/build
$pkgdir/install.sh $dist $pkgdir/requirements.txt
version=$(cd /; $DESTDIR/opt/cornac/bin/python -c 'import pkg_resources as p; print(p.get_distribution("pgCornac").version)')

rpm=$(readlink -m $distdir/cornac-$version-1.x86_64.rpm)
fpm --verbose \
    --force \
    --debug-workspace \
    --workdir=$workdir \
    --input-type dir \
    --output-type rpm \
    --package $rpm \
    --name cornac \
    --version $version \
    --iteration 1 \
    --architecture amd64 \
    --description "RDS-compatible Managed-Postgres Webservice" \
    --category database \
    --license PostgreSQL \
    --depends libev \
    --depends libvirt-libs \
    --depends python36 \
    --after-install $pkgdir/postinst.sh \
    --after-remove $pkgdir/postrm.sh \
    $DESTDIR/=/

# TEST

test -f $rpm
chown $(stat -c %u:%g $0) $rpm

yum install -qy $rpm
(cd /; /opt/cornac/bin/cornac --version)

ln -fs ${rpm##*/} ${rpm%/*}/cornac-last.rpm
