from pkg_resources import iter_entry_points

from ..errors import KnownError


def factory(iaas, config):
    name = config['OPERATOR']
    try:
        ep, *_ = iter_entry_points('cornac.operators', name=name)
    except ValueError:
        raise KnownError(f"Unknown operator type {name}")

    cls = ep.load()
    return cls(iaas, config)
