# -*- shell-script -*-
# Kickstart2 script to setup a CentOS7 appliance for Cornac Web service.
#
# See reference at https://docs.centos.org/en-US/centos/install-guide/Kickstart2/#sect-kickstart-syntax.
#

# Don't hang on error. Let's build script manage errors.
%onerror --erroronfail
shutdown -h now
%end

# Ensure network install, instead of searching for a CDROM.
url --url=http://mirror.i3d.net/pub/centos/7/os/x86_64/

# Ensure installation is non-interactive.
cmdline
# Poweroff after installation.
poweroff

#       D I S K

clearpart --all --drives=sda --initlabel
zerombr
part /boot	--label=boot	--asprimary	--size=512	--fstype=ext2
part swap	--label=swap	--asprimary	--size=4096	--fstype=swap
part pv.0	--label=lvmpv0	--asprimary	--size=1	--grow
volgroup cornac pv.0
logvol /	--vgname cornac	--name root	--size=1	--grow	--fstype=ext4

#       N E T W O R K

network --device=link --activate --bootproto=dhcp --hostname=cornac
firewall --enabled --ssh --http --service https

#       L O C A L E

keyboard us
lang en_US --addsupport=fr_FR
timezone Europe/Paris --ntpservers=0.pool.ntp.org,1.pool.ntp.org,2.pool.ntp.org,3.pool.ntp.org

#      P A C K A G E S

%packages
-aic94xx-firmware
-alsa-*
-btrfs-progs
-ivtv-firmware
-iwl*firmware
-man-db
-plymouth*
-xfsprogs
-wpa_supplicant
open-vm-tools
%end
skipx

#       S Y S T E M

bootloader --location=mbr --timeout 2 --password C0rn@c
rootpw C0rn@c

%post --interpreter=/bin/bash --log=/var/log/cornac-kickstart-post.log
set -eux
# Installation was on ttyS0, but not runtime.
sed -i '/^GRUB_CMDLINE_LINUX=/s/ console=ttyS0//' /etc/default/grub
grub2-mkconfig > /etc/grub2.cfg

# Streamline initramfs and ensure VMWare drivers are embedded.
cat >/etc/dracut.conf.d/cornac.conf <<EOF
omit_dracutmodules+="mdraid plymouth"
# Ensure VMWare drivers are included.
add_drivers+="vmxnet3 vmw_balloon vmwgfx vmw_pvscsi vmw_vmci vmw_vsock_vmci_transport"
EOF
dracut --force --verbose

# Setup YUM and extra repositories
sed -i /^keepcache=/s/=0/=1/ /etc/yum.conf
yum install -y epel-release https://yum.dalibo.org/labs/dalibo-labs-2-1.noarch.rpm
yum makecache fast

# Install cornac stack
yum install -y cornac nginx

# Make some room.
(
    find /usr/share/locale/* | grep -Ev 'en_US|fr';
    find /usr/share/man/* | grep -v '/man/man';
) | xargs rm -rf

# Notify success to build script.
: SUCCESS
%end
