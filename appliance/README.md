# Cornac Webservice Virtual Appliance

This sub-project provides scripts to build, export and deploy Cornac Webservice
as a standalone virtual appliance.

The build requires `libvirt-qemu`, `virt-install`, `libguestfs-tools`, a local
bridge with DHCP like `lxcbr0` or `virbr0` and 4GB of disk space available. You
don't need to be root as long as your setup allows to run VM as user.

The `Makefile` is the main entrypoint for managing the project. `make ova`
builds `cornac.ova` ready for deployment on vCenter. Here are available make
commands:

- `cache` -- Sets up an HTTP proxy cache with docker-compose. See below.
- `clean` -- Clean `cornac.ova` and origin VM and disks.
- `ova` -- Builds `cornac.ova`.


## Deploying to vCenter

Once cornac.ova is ready, deploy it using vSphere Web Client. You can also use
`deploy_ova.py` script, adapted from `pyvmomi` community samples. This script
requires a cornac webservice development environment. Run it as following:

``` console
$ ./deploy_ova.py --disable_ssl_verification --host vsphere.lan.company.tld --user me@vsphere.local
```

See `./deploy_ova.py --help` for further details.

Once appliance is up and running, access it through SSH as root with default
password `C0rn@c`. The default hostname is `cornac`.


## Caching Network Traffic

Building `cornac.ova` requires more than 800MB of download. While maintaining
appliance scripts, this is a big waste of bandwidth and time. `make cache` sets
up a [Polipo](https://www.irif.fr/~jch/software/polipo/) HTTP proxy cache,
exposed as `cornac-proxy-cache.docker`. Use it like this:

``` console
$ http_proxy=http://cornac-proxy-cache.docker:8123 make ova
```

The average time for building a virtual appliance with warm cache is 6 minutes.
