#!/bin/bash -eux
#
# Builds a VM called cornac-appliance, using libvirt.
#

set -o pipefail

find_bridge() {
	local bridges=$(readlink -e /sys/devices/virtual/net/*/bridge | grep -Ev '/br-.*|/docker0')
	bridges=($bridges)

	if [ "${#bridges[@]}" -eq 0 ] ; then
		echo "No network bridge found." >&2
		return 1
	elif [ "${#bridges[@]}" -gt 1 ] ; then
		echo "Several network bridges. Can't choose one." >&2
		return 1
	else
		local bridge="$(readlink -m ${bridges[0]}/..)"
		bridge="${bridge##*/net/}"
		echo "Using network ${bridge}." >&2
		echo "$bridge"
	fi
}

find_location() {
	local ips=$(hostname --all-ip-addresses)
	ips=($ips)
	echo http://${ips[0]}:8000/installer
}

# Commented list of boot cmdline.
boot_cmdline=(
	quiet
	console=ttyS0
	ip=dhcp
	plymouth.enable=0
	# Comment this to re-enable emergency shell on error.
	rd.shell=0 rd.emergency=halt

	# Limit dracut timeout since we're on virtual system.
	rd.retry=3 rd.timout=15

	# Define Kickstart script.
	ks=file:/cornac.ks

	# For anaconda
	inst.cmdline
	inst.headless
)

if [ -n "${http_proxy-}" ] ; then
	boot_cmdline+=(proxy=${http_proxy})
fi

if ! bridge=$(find_bridge) ; then
	exit 1
fi

# Commented list of virt-install arguments.
virtinstall_args=(
	--debug

	# Various system specs.
	--name cornac-appliance
	--os-type linux
	--os-variant centos7.0
	--features acpi=on,apic=on
	--vcpus 2
	# This is the minimum for anaconda to run properly.
	--ram 2048
	--memballoon virtio

	--network bridge=${bridge}

	# Enable graphics, but use console for headless installation.
	--console pty,target_type=serial
	--graphics spice
	--video qxl
	--channel spicevmc

	# Add 4G disk on a paravirt SCSI bus
	--disk bus=scsi,size=8,format=qcow2,sparse=yes
	--controller scsi,model=virtio-scsi

	# Download installer and packages instead of using an ISO.
	--location http://mirror.i3d.net/pub/centos/7/os/x86_64/

	# Inject kickstart script.
	--initrd-inject "${PWD}/cornac.ks"

	# Ensure VM is down after installation.
	--noreboot

	--extra-args "${boot_cmdline[*]}"

	# Return right after VM is launched we'll stream installation output
	# from text console.
	--noautoconsole

)

# Trigger installation
virt-install "${virtinstall_args[@]}"

# Watch installation in TTY, wait for shutdown.
virsh console cornac-appliance --force

# Show and check post install script success.
virt-cat --domain cornac-appliance /var/log/cornac-kickstart-post.log | tee /dev/stderr | grep -q SUCCESS

# Export compress disk as vmdk for ova.
diskfile=$(virsh dumpxml cornac-appliance | grep -Po "file='\K.*/cornac-appliance.qcow2")
virt-sparsify $diskfile --convert vmdk -o adapter_type=lsilogic,subformat=streamOptimized,compat6 cornac-sda.vmdk
