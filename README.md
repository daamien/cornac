# cornac - Enterprise-class Postgres deployment

The project is subdivided as following:

- `appliance/` is a libvirt/kickstart project to build Virtual appliance as OVA.
- `origin/` is an Ansible project to maintain Postgres host template.
- `service/` is a Python project implementing RDS-compatible webservice.
